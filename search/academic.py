# -*- coding: utf-8 -*-

from elasticsearch import Elasticsearch, RequestsHttpConnection, helpers
from elasticsearch_dsl import Search
from requests_aws4auth import AWS4Auth
import os
import sys

if os.environ.get("DEBUG") == "false":
    file = "/code/cloudsearch"
else:
    file = "../cloudsearch"
sys.path.insert(1, file)

from search.search import parse_text

# os.environ["URL"] = "search-academic-nqus5nfznmd7uk6e7qlctpz4zy.us-east-1.es.amazonaws.com"
# os.environ["ACCESS"] = "AKIAIXLO7KWULQAFFA5Q"
# os.environ["SECRET"] = "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"
# os.environ["REGION"] = "us-east-1"
# os.environ["TOKEN"] = ""

os.environ["URL"] = "30ae9d3b1e7f433ba5883987961f530e.us-east-1.aws.found.io"
os.environ["USER"] = "elastic"
os.environ["PASS"] = "OORdNGmDIexOwk981ZKUASMd"


class Config:

    def __init__(self):
        self.user = ""
        self.password = ""
        self.region = ""
        self.session_token = ""
        self.url = ""
        self.x = 5

    def get_config(self):
        self.user = os.getenv("USER")
        self.password = os.getenv("PASS")
        self.region = os.getenv("REGION")
        self.session_token = os.getenv("TOKEN")
        self.url = os.getenv("URL")


class SearchElasticSearch(Config):

    def __init__(self):
        Config.__init__(self)
        self.es = Elasticsearch()
        self.get_config()
        self.activate_es()

    def activate_es(self):
        auth = (self.user, self.password)
        self.es = Elasticsearch(
            hosts=[{'host': self.url, 'port': 9243}],
            http_auth=auth,
            scheme="https",
            ssl=True,
        )

    def search(self, search, field, index='arxiv'):
        """

        :param search: Text to search for
        :param field: Field name to search
        :param index: Index to search
        :return: Array of hits. Remains in object form to access field attributes
        """
        s = Search(index=index).using(self.es).query("match", **{field: search})
        response = s.execute()
        if len(response) > 10:
            return response[:10], None
        if len(response) == 0:
            return None, "No results found"
        return response, None

    @staticmethod
    def handle_response(resp):
        # TODO How to page through dozens of search results
        articles = []
        err = None
        for hit in resp:
            articles.append(hit.body)
        if len(articles) > 10:
            return articles[:10], err
        return articles, err





# results, err = es.search("photo collider", "body")
# if err is not None:
#     print(err)
# else:
#     print(results[0].body)

# es.upload_academic("am I a new docu", "blah 3", "2015", "derp who", "what what")
#
# status_code = 200
# i = 1


# s = Search(using=es.es, index="arxiv") \
#     .query("match", author="author")   \

# s = Search().using(es.es).query("match", body="EIT-based optical magnetometer")
# response = s.execute()
#
# for hit in s[:3]:
#     print(hit.body)

# print(response.__len__())
# for hit in response:
#     print(hit)

# helpers.bulk(es, es.gendata())



