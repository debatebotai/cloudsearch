import requests
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import sys
import logging
import os

if os.environ.get("DEBUG") == "false":
    file = "/code/cloudsearch"
else:
    file = "../cloudsearch"
sys.path.insert(1, file)

from search.search import parse_text

logger = logging.getLogger(__name__)
STOP_WORDS = set(stopwords.words('english'))
STOP_WORDS.add('to')
STOP_WORDS.add('the')

# TODO How do we add more fields quickly. It should be easy to add more fields and not break other apps that depend

# Score based on matching phrases/terms
# TODO Return top 3 results
# TODO Need a general search function with fields, url as arguments

# TODO Should i do term or phrase? Term may actually be better. BUt may want to expand to do both and check for unique
# ids


def article_search_cloud(text):
    text = text.strip(''' " ''')
    text_tokens, trigrams = parse_text(text, tokenize=True, trigrams=True)

    phrase_q = '''(or '''
    if len(trigrams) > 25:
        trigrams = trigrams[0:24]
    for i in trigrams:
        phrase = ' '.join(i)
        phrase_q = phrase_q + "(near+field%s'raw_text'+distance%s3+'%s') " % ('%3D', '%3D', phrase)
    phrase_q = phrase_q + ")&q.parser=structured&return=raw_text,url&size=2"
    base_url = \
        'http://search-asdfasdf-7tsnrzx3kxczrlxmdzw5y3tzdy.us-east-1.cloudsearch.amazonaws.com/2013-01-01/search?q='

    r = requests.get(base_url + phrase_q)
    try:
        json_resp = r.json()
    except Exception as e:
        return None, {"error": "error converting response from search to json. "}
    return json_resp, None



