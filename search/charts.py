import requests
import logging
import sys
import os

if os.environ.get("DEBUG") == "false":
    file = "/code/cloudsearch"
else:
    file = "../cloudsearch"
sys.path.insert(1, file)

from search.search import parse_text, handle_charts_response


def get_charts(text):
    base_url = \
        'http://search-asdfasdf-7tsnrzx3kxczrlxmdzw5y3tzdy.us-east-1.cloudsearch.amazonaws.com/2013-01-01/search?q='
    text = text.strip(''' " ''')
    text_tokens = parse_text(text, tokenize=True, trigrams=False)

    term_q = '''(or '''
    if len(text_tokens) > 20:
        for i in text_tokens[0:20]:
            term_q = term_q + "(term+field%s'image_tags'+'%s') " % ('%3D', i)
    else:
        for i in text_tokens:
            term_q = term_q + "(term+field%s'image_tags'+'%s') " % ('%3D', i)

    term_q = term_q + ')&q.parser=structured&return=image_link&size=2'
    r = requests.get(base_url + term_q)
    try:
        resp = r.json()
        urls, error = handle_charts_response(resp)
    except Exception as e:
        return None, {"error": "Error processing chart search response"}
    if error:
        return None, error
    return urls, error


images, error = get_charts("je averag")
