import requests
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

import logging

logger = logging.getLogger(__name__)
STOP_WORDS = set(stopwords.words('english'))
STOP_WORDS.add('to')
STOP_WORDS.add('the')


# def handle_article_response(resp):
#     articles = []
#     near_hits = resp.get('near_hits')
#     if near_hits.get('error'):
#         logging.info("Error with near hits: %s and %s", near_hits.get('error'))
#         return None, {"error": "Error for near hits", "resp": resp}
#
#     near_hits = resp.get('near_hits').get('hits').get('hit')
#     for i in range(len(near_hits)):
#         url = near_hits[i]['fields']['url']
#         articles.append((url, near_hits[i]['fields']['raw_text']))
#
#     return articles, resp


def handle_article_response(resp):
    articles = []
    near_hits, err = handle_response(resp)
    if err:
        return None, err

    for i in range(len(near_hits)):
        url = near_hits[i]['fields']['url']
        articles.append((url, near_hits[i]['fields']['raw_text']))
    return articles, None


def handle_response(resp):
    hits = resp.get("hits").get("hit")
    if not hits:
        return None, {"error": "Error retrieving search results.", "resp": resp}

    return hits, None


def handle_charts_response(resp):
    hits, error = handle_response(resp)
    urls = []
    if len(hits) == 0:
        return None, {"error": "no charts matched"}
    for i in hits:
        u = i.get("fields").get("image_link")
        urls.append(u)
    return urls, None


def parse_text(text, title=False, article=False, tokenize=False, trigrams=False):
    # If not already tokenized
    # TODO Clean text better for search
    if text and tokenize:
        text = text.lower()
        text = text.replace("'", "")
        text = text.replace('''"''', "")
        text = text.replace('\n', "")
        text = text.replace('\\', "")
        text = text.replace(r'\\', "")
        text = text.replace('\n\n', "")
        text = text.replace("(", "")
        text = text.replace(")", "")
        text = text.strip("\n")
        text_tokens = word_tokenize(text)
        for word in text_tokens:
            if word in STOP_WORDS:
                text_tokens.remove(word)
            else:
                continue
        text = text_tokens
    else:
        for word in text:
            if word in STOP_WORDS:
                text.remove(word)
            else:
                continue
    # new_title = ' '.join(title_tokens)
    # Generate Trigrams for cloudsearch fuzzy phrase matching
    if trigrams:
        trigram_tokens = []
        for i in range(0, len(text), 3):
            try:
                trigram_tokens.append((text[i: i + 3]))
            except IndexError:
                try:
                    trigram_tokens.append((text[i: i + 2]))
                except IndexError:
                    try:
                        trigram_tokens.append((text[i: i + 1]))
                    except IndexError:
                        trigram_tokens.append((text[-1:-4]))
        return text, trigram_tokens
    else:
        return text
